[TOC]

# Custom Vagrant Box - CentOS 7 [Perun] #

-----

* Instalirati VirtualBox [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads) (testirana verzija 4.3.30)

* Instalirati Vagrant [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html) (testirana verzija 1.7.2)

-----

## Pre kloniranja repo-a (samo Windows) ##

Postoji problem sa novim linijama http://en.wikipedia.org/wiki/Newline

Potrebno je izvršiti komandu `git config --global core.autocrlf input`.

Ili, peške, locirati .gitconfig fajl (home folder korisnika) i dodati sledeće:

```
#!ini
[core]
    autocrlf = input

```

## Kloniranje repo-a ##

Repo gde se nalazi ovaj fajl.

## RSA ključevi ##

[https://help.github.com/articles/generating-ssh-keys](https://help.github.com/articles/generating-ssh-keys)

Ovaj korak je moguće ponoviti kasnije. Komanda: `bash /vagrant/setup/ssh_keys.sh`.

*Primer 1:*

```
dot/.ssh/bitbucket/id_rsa
dot/.ssh/bitbucket/id_rsa.pub
dot/.ssh/config
```

Sadržaj fajla `dot/.ssh/config`:

```
Host bitbucket.org
    IdentityFile ~/.ssh/bitbucket/id_rsa
```

*Primer 2:*

Ukoliko je potrebno dodati novi ključ za bitbucket, a da pri tome i stari bude u funkciji.

```
dot/.ssh/bitbucket/id_rsa
dot/.ssh/bitbucket/id_rsa.pub
dot/.ssh/bitbucket_project1/id_rsa
dot/.ssh/bitbucket_project1/id_rsa.pub
dot/.ssh/config
```

Sadržaj fajla `dot/.ssh/config`:

```
Host bitbucket.org
    IdentityFile ~/.ssh/bitbucket/id_rsa
Host bitbucket_project1
    HostName bitbucket.org
    IdentityFile ~/.ssh/bitbucket_project1/id_rsa
```

U ovom slučaju, obavezno umesto `butbucket.org` kucati `bitbucket_project1` u adresi za kloniranje repo-a. Ukoliko je repo kloniran ranije, setovati izmenjen repo url `git remote set-url origin git@bitbucket_project1:lorem/ipsum.git`

Napomena: drugi primer je za slučaj kada postoje dva ključa za isti host (npr. bitbucket.org). Ukoliko se dodaje ključ za novi host (npr. github.com), dovoljno je ispratiti prvi primer i nije potrebno menjati git repo url.


## Test sa lokalne mašine ##

Dodati u lokalni hots fajl `192.168.33.10 vagrant-test.dev`

http://vagrant-test.dev

http://vagrant-test.dev/phpMyAdmin (korisnik:`root` šifra: `vagrant`)

http://vagrant-test.dev/smtpcatcher

## Mount (lokalni folder) ##

*Linux/Unix:*

`mount -t nfs 192.168.33.10:/var/www /lokalni/folder/www`.

*Windows:*

1. [Install Client for NFS](http://www.home.agilent.com/agilent/editorial.jspx?cc=RS&lc=eng&ckey=2106090&nid=-11143.0.00&id=2106090)

2. [Enable write for NFS share](http://www.ibmconnections.org/wordpress/index.php/2013/02/configure-windows-nfs-client-with-write-permissions/) - uneti 1000 za Gid i 1000 za Uid

3. Restartovati računar

4. [Folder mount](http://gentoo-blog.de/ubuntu/mount-a-linux-nfs-share-on-windows-7/) - `mount \\192.168.33.10\var\www z:`

Alternativno rešenje za Windows: https://code.google.com/p/nekodrive/
