#!/bin/bash


# bash /vagrant/install_project.sh --git_url="git@host.com:user/repo.git" --git_checkout="dev" --install_dir="/var/www/project.dev"

GIT_URL=""
GIT_CHECKOUT=""
INSTALL_DIR=""

usage()
{
cat << EOF
--------------------------------------------------------------------------------
usage: $0 options

This script will clone repo and run install script from project

OPTIONS:
   -h, --help       Show this message
   --git_url        Git repo url (git@host.com:user/repo.git)
   --git_checkout   Branch, tag or commit id. Leave empty for master branch
   --install_dir    Internal dir path for project (/var/www/project.dev)
--------------------------------------------------------------------------------
EOF
}

while :
do
    case $1 in
        --git_url=*)
            GIT_URL=${1#*=}
            shift
            ;;
        --git_checkout=*)
            GIT_CHECKOUT=${1#*=}
            shift
            ;;
        --install_dir=*)
            INSTALL_DIR=${1#*=}
            shift
            ;;
        --)
            shift
            break
            ;;
        -*)
            usage
            exit
            break
            ;;
        *)
            break
            ;;
    esac
done

if [ ! "$GIT_URL" ]; then
    echo "ERROR"
    echo "missing git url"
    echo
    usage
    exit 1
fi

if [ ! "$INSTALL_DIR" ]; then
    echo "ERROR"
    echo "missing install dir"
    echo
    usage
    exit 1
fi

if [ -d "$INSTALL_DIR" ]; then
    echo "install dir already exists"
    exit 1
fi

mkdir "$INSTALL_DIR"
cd "$INSTALL_DIR"
git clone "$GIT_URL" .
if [ "$GIT_CHECKOUT" ]; then
    git checkout "$GIT_CHECKOUT"
fi

if [ ! -f "$INSTALL_DIR/scripts/vagrant/install.sh" ]; then
    echo "missing install script"
    exit 1
fi

bash "$INSTALL_DIR/scripts/vagrant/install.sh"
