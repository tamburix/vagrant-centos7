# 1.0.2  (unreleased)

BACKWARDS INCOMPATIBILITIES:

  - /

FEATURES:

  - /

IMPROVEMENTS:

  - Better ssh keys setup

  - Missing folder /var/www/logs

  - Xdebug config

BUG FIXES:

  - Missing Node.js and npm

# 1.0.1  (August 3, 2015)

BACKWARDS INCOMPATIBILITIES:

  - /

FEATURES:

  - SMTP Mail Catcher

  - Upgrade scripts for Vagrant box

IMPROVEMENTS:

  - readme.md

BUG FIXES:

  - /

# 1.0.0  (August 3, 2015)

BACKWARDS INCOMPATIBILITIES:

  - /

FEATURES:

  - LAMP, Ruby, Node.js, NFS Server and lots of development tools

IMPROVEMENTS:

  - /

BUG FIXES:

  - /
