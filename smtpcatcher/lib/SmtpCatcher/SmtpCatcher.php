<?php

namespace SmtpCatcher;

class SmtpCatcher
{
    protected $mail = '';
    protected $raw  = '';

    public function catchMail()
    {
        $this->parseMail(file_get_contents('php://stdin', 'r'));
        $this->writeFile();
    }

    public function serveMail()
    {
        echo "Visit: http://localhost:8100\n";
        chdir(__DIR__ . '/../../www');
        exec('php -S localhost:8100');
    }

    protected function parseMail($raw)
    {
        $this->raw = $raw;

        $headers          = iconv_mime_decode_headers($raw, 0, 'UTF-8');
        $to               = '';
        $from             = '';
        $subject          = '';

        if (isset($headers['To'])) {
            $to = $headers['To'];
        }
        if (isset($headers['From'])) {
            $from = $headers['From'];
        }
        if (isset($headers['Subject'])) {
            $subject = $headers['Subject'];
        }

        $message = explode(PHP_EOL . PHP_EOL, $raw);
        if (count($message) === 2) {
            $message = $message[1];
        } else {
            $message = '';
        }

        $this->mail = compact('headers', 'to', 'from', 'subject', 'message', 'raw');
    }

    protected function writeFile()
    {
        $time = time();
        if (file_exists(__DIR__ . '/../../cache/database.json')) {
            $database = json_decode(file_get_contents(__DIR__ . '/../../cache/database.json'), true);
            $database[$time] = $this->mail;
        } else {
            $database = [];
            $database[$time] = $this->mail;
        }

        file_put_contents(__DIR__ . '/../../cache/database.json', json_encode($database));
        file_put_contents(__DIR__ . '/../../cache/raw-' . $time . '.txt', $this->raw);
    }
}
