<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <title>SmtpCatcher</title>
    <style type="text/css">
        body{
            margin: 0px;
        }
        h3{
            margin: 0px;
            padding: 10px 0px;
        }
        h4{
            margin: 0px;
        }
        .mail-wrap{
            padding: 5px;
            border-bottom: 1px solid gray;
            overflow: auto;
        }
        .odd{
            background-color: #EFF5FF;
        }
        a, a:visited{
            color: blue;
        }
        a:active{
            color: red;
        }
    </style>
</head>
<body>
    <?php $i = 0; ?>
    <?php foreach ($emails as $timestamp => $data): ?>
        <div class="mail-wrap <?php if ($i++%2 === 0): ?>odd<?php endif; ?>">
            <h3><?php echo date('l jS \of F Y h:i:s A', $timestamp); ?></h3>
            <h4>To: <?php echo htmlentities($data['to']); ?></h4>
            <h4>From: <?php echo htmlentities($data['from']); ?></h4>
            <h4>Subject: <?php echo htmlentities($data['subject']); ?></h4>
            <pre><?php echo quoted_printable_decode($data['message']); ?></pre>
            <p><a href="raw.php?id=<?php echo $timestamp; ?>">View Raw</a></p>
        </div>
    <?php endforeach; ?>
</body>
</html>
