#!/bin/bash


GIT_USER_NAME=`php /vagrant/setup/random_user.php -n`
GIT_USER_EMAIL=`php /vagrant/setup/random_user.php -e`
echo "
 ____  _     _____    _    ____  _____
|  _ \| |   | ____|  / \  / ___|| ____|
| |_) | |   |  _|   / _ \ \___ \|  _|
|  __/| |___| |___ / ___ \ ___) | |___
|_|   |_____|_____/_/   \_\____/|_____|
 ____  _____    _    ____    _   _   _
|  _ \| ____|  / \  |  _ \  | | | | | |
| |_) |  _|   / _ \ | | | | | | | | | |
|  _ <| |___ / ___ \| |_| | |_| |_| |_|
|_| \_\_____/_/   \_\____/  (_) (_) (_)
"
echo
echo "please remember to change git user name and email:"
echo
echo "$ git config --global user.name \"J. Random Hacker\""
echo "$ git config --global user.email \"j.random@hack.com\""
echo
sudo -u vagrant git config --global user.name "$GIT_USER_NAME"
sudo -u vagrant git config --global user.email "$GIT_USER_EMAIL"
echo "your current random name: \"$GIT_USER_NAME\", and email: \"$GIT_USER_EMAIL\""
echo
echo "https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup"
echo "
 ____  _     _____    _    ____  _____
|  _ \| |   | ____|  / \  / ___|| ____|
| |_) | |   |  _|   / _ \ \___ \|  _|
|  __/| |___| |___ / ___ \ ___) | |___
|_|   |_____|_____/_/   \_\____/|_____|
 ____  _____    _    ____    _   _   _
|  _ \| ____|  / \  |  _ \  | | | | | |
| |_) |  _|   / _ \ | | | | | | | | | |
|  _ <| |___ / ___ \| |_| | |_| |_| |_|
|_| \_\_____/_/   \_\____/  (_) (_) (_)
"
echo
