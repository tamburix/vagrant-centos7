<?php


function usage(){
    return <<<EOF

usage: script options

OPTIONS:
-n      Name (default)
-e      Email
-u      Username
-h      Help (this message)


EOF;
}

$opts = getopt('hneu');
if (isset($opts['n'])) {
    $make = 'name';
} elseif (isset($opts['e'])) {
    $make = 'email';
} elseif (isset($opts['u'])) {
    $make = 'username';
} else {
    echo usage();
    exit;
}

$names = array(
'snarky', 'bowyang', 'floppy', 'gullet', 'gallivant', 'quack', 'rubbish', 'reservoir', 'collop', 'tweezers', 'lozenge', 'inevitable', 'hocuspocus', 'fartlek', 'pony', 'nambypamby', 'moose', 'hoosegow', 'spam', 'borborygm', 'sponge', 'bowl', 'vomitory', 'gauze', 'oblong', 'pudding', 'logorrhea', 'fatuous', 'squeegee', 'rustic', 'burgoo', 'furphy', 'gonzo', 'baffled', 'quibble', 'graze', 'sandals', 'diphthong', 'queer', 'feeble', 'petcock', 'goon', 'lugubrious', 'shenanigan', 'cummerbund', 'troglodyte', 'anomalous', 'dudgeon', 'malarkey', 'hootenany'
);

$out = '';
switch ($make) {
    case 'name':
        $out = ucfirst($names[rand(0, count($names) - 1)]) . ' ' . ucfirst($names[rand(0, count($names) - 1)]);
        break;
    case 'email':
        $out = $names[rand(0, count($names) - 1)] . '.' . $names[rand(0, count($names) - 1)] . '@' . $names[rand(0, count($names) - 1)] . '.com';
        break;
    case 'username':
        $out = $names[rand(0, count($names) - 1)] . '_' . $names[rand(0, count($names) - 1)];
        break;
    default:
        break;
}

echo $out;
