
# NETWORK

Set `ONBOOT="yes"` in file `/etc/sysconfig/network-scripts/ifcfg-enp0s3`

```
echo "vagrant-centos7" > /etc/hostname
echo "127.0.0.1 vagrant-centos7" >> /etc/hosts
echo "::1 vagrant-centos7" >> /etc/hosts
```

# SUDO

`$ groupadd admin`
`$ usermod -G admin vagrant`
`$ visudo`
Add `SSH_AUTH_SOCK` to the env_keep option (deprecated?)
Comment out the `Defaults requiretty line`
Add the line `%admin ALL=NOPASSWD: ALL`

# SELINUX

Set `SELINUX=disabled` in file `/etc/selinux/config`

# FIREWALL

`$ systemctl stop firewalld.service`
`$ systemctl disable firewalld.service`

restart

# UPDATE SYSTEM

`$ yum update`

restart

# REMOVE OLD KERNELS

Reduce box size.

`yum install -y yum-utils`

`package-cleanup --oldkernels --count=1`

Set `installonly_limit=2` in file `/etc/yum.conf`

# VIRTUALBOX ADDITIONS

```
$ yum groupinstall "Development Tools"
$ mkdir /media/vba
$ mount -r /dev/cdrom /media/vba
$ /media/vba/VBoxLinuxAdditions.run
```

# VAGRANT KEY

`$ curl -k https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub > /home/vagrant/.ssh/authorized_keys`

```
cd /home/vagrant
mkdir .ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant ecure public key" > .ssh/authorized_keys
chmod 0700 .ssh
chmod 0600 .ssh/authorized_keys
