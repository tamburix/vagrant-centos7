#/bin/bash


wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
wget http://rpms.remirepo.net/enterprise/remi-release-7.rpm
rpm -Uvh remi-release-7.rpm epel-release-latest-7.noarch.rpm

yum --enablerepo=remi,remi-php56 install -y mariadb-server mariadb httpd httpd-devel mod_ssl php php-devel php-cli php-gd php-imap php-ldap php-odbc php-pear php-xml php-xmlrpc php-pecl-apc php-mbstring php-mcrypt php-mysql php-mssql php-snmp php-soap php-tidy perl-libwww-perl ImageMagick libxml2 libxml2-devel mod_fcgid php-pecl-xdebug

systemctl start httpd.service
systemctl enable httpd.service

systemctl start mariadb.service
systemctl enable mariadb.service

# mysql
/usr/bin/mysqladmin -u root password 'vagrant'
mysql --password=vagrant --user=root mysql -e "CREATE USER 'root'@'%';"
mysql --password=vagrant --user=root mysql -e "GRANT ALL PRIVILEGES ON * . * TO  'root'@'%' IDENTIFIED BY 'vagrant' WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;"
mysql --password=vagrant --user=root mysql -e "GRANT ALL PRIVILEGES ON * . * TO  'root'@'localhost.localdomain' IDENTIFIED BY 'vagrant' WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;"
mysql --password=vagrant --user=root mysql -e "GRANT ALL PRIVILEGES ON * . * TO  'root'@'127.0.0.1' IDENTIFIED BY 'vagrant' WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;"
mysql --password=vagrant --user=root mysql -e "GRANT ALL PRIVILEGES ON * . * TO  'root'@'::1' IDENTIFIED BY 'vagrant' WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;"

# php
chown -R vagrant:vagrant /var/lib/php/session
echo "date.timezone = Europe/Belgrade" >> /etc/php.ini

# apache
mkdir -p /etc/httpd/cert
echo "-----BEGIN CERTIFICATE-----
MIIDADCCAegCCQDITKleiRUfMjANBgkqhkiG9w0BAQUFADBCMQswCQYDVQQGEwJY
WDEVMBMGA1UEBwwMRGVmYXVsdCBDaXR5MRwwGgYDVQQKDBNEZWZhdWx0IENvbXBh
bnkgTHRkMB4XDTE0MDkwMzEyMDAzN1oXDTE1MDkwMzEyMDAzN1owQjELMAkGA1UE
BhMCWFgxFTATBgNVBAcMDERlZmF1bHQgQ2l0eTEcMBoGA1UECgwTRGVmYXVsdCBD
b21wYW55IEx0ZDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALuA44Uw
rBYbHZnxEpl5m9b0HDb5eKnp12UzF4rzNZz63GE+fvFnjyr/t9oHZD4enUFcKitD
XjvlDucgjMhQ7fBSRjEncBAVGASTH9TCFCxotNhKvZFm6IUhkncq6i0W5xmvcSeY
70gQ08XHEC39HVLiO7mrR+AHrywXXOz1wyfyT78Y2CW8gxjgjH79GuOAUo4o4Wxs
SfwtXZcF5kUaOItOYnVqIChD61SnhRxTNuJy6ZjldtBeVfjS2DqWnq7OabNjEN4k
goJ2cxjqTdn/z4Y73M/DmHCdhB8ew0/3DukPqbP5GQDEu+17YJlx3/p5xu/LoCAV
x/6cE8Tt3QUEcXMCAwEAATANBgkqhkiG9w0BAQUFAAOCAQEAJzYZ22FmcfdUyfTm
gezOc/z/qZAR11dMQp140+ahB2C9fusl+Om9fGibuwW0fzPrAoTzix26PMokVwUW
adcygcKMwGlBOKKKuS+fSX3zSUxyh/b50bXfnq2G+q28316IHeWeLzPTE3rFKgns
Jj5VQVKPCRKxdEy6BQPpNmuFSjwIpAK66HzW25W+9/0qYF4yXZBEQWui3ZxAT/KC
dYc7RsBLyXyjCT7fwivxcuFlEdWSItg6bTzm3rNASnslIiKek/7VnqdT6J5Hjw9T
LZ0DmUSTHOaO8M4Yc7SNy0LscZMoAwmIGFJ6KrJzYElnlGRmTlLYFZ/QAPHBfPvP
r/kdPQ==
-----END CERTIFICATE-----" > /etc/httpd/cert/ca.crt
echo "-----BEGIN CERTIFICATE REQUEST-----
MIIChzCCAW8CAQAwQjELMAkGA1UEBhMCWFgxFTATBgNVBAcMDERlZmF1bHQgQ2l0
eTEcMBoGA1UECgwTRGVmYXVsdCBDb21wYW55IEx0ZDCCASIwDQYJKoZIhvcNAQEB
BQADggEPADCCAQoCggEBALuA44UwrBYbHZnxEpl5m9b0HDb5eKnp12UzF4rzNZz6
3GE+fvFnjyr/t9oHZD4enUFcKitDXjvlDucgjMhQ7fBSRjEncBAVGASTH9TCFCxo
tNhKvZFm6IUhkncq6i0W5xmvcSeY70gQ08XHEC39HVLiO7mrR+AHrywXXOz1wyfy
T78Y2CW8gxjgjH79GuOAUo4o4WxsSfwtXZcF5kUaOItOYnVqIChD61SnhRxTNuJy
6ZjldtBeVfjS2DqWnq7OabNjEN4kgoJ2cxjqTdn/z4Y73M/DmHCdhB8ew0/3DukP
qbP5GQDEu+17YJlx3/p5xu/LoCAVx/6cE8Tt3QUEcXMCAwEAAaAAMA0GCSqGSIb3
DQEBBQUAA4IBAQAZdnCafT1KXEKoqI3+H01hSAf1VoHIWwCGYOHDXPuvgNtXukI7
r6VVL6eX+R9+kBPhhwRj0cCoYNGYX5fB/4T7F5mnrhMT1Y7Uozk5ijh0YoYDkYvW
J8Pz9TAplbnteZ6D0XdsmHLcfXeVi2E2GuvyLUxKaygZwaBuYGcE1pwOnvr1ylQa
8tMoxQz/8dnZLfXOm6scznk4BhXEi1auLa47XqkTCQe1zT6DyETXH2ne5uYPxd4k
9/aDhrnLVppQ3TH005Y4aRiCY1SHymSPpE0aAE6FIdesrBtDam2vecGxME1/MO3v
jCPF+DJo8FSh6kAyysoDBATbq4pzcAi7XAtG
-----END CERTIFICATE REQUEST-----" > /etc/httpd/cert/ca.csr
echo "-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAu4DjhTCsFhsdmfESmXmb1vQcNvl4qenXZTMXivM1nPrcYT5+
8WePKv+32gdkPh6dQVwqK0NeO+UO5yCMyFDt8FJGMSdwEBUYBJMf1MIULGi02Eq9
kWbohSGSdyrqLRbnGa9xJ5jvSBDTxccQLf0dUuI7uatH4AevLBdc7PXDJ/JPvxjY
JbyDGOCMfv0a44BSjijhbGxJ/C1dlwXmRRo4i05idWogKEPrVKeFHFM24nLpmOV2
0F5V+NLYOpaers5ps2MQ3iSCgnZzGOpN2f/Phjvcz8OYcJ2EHx7DT/cO6Q+ps/kZ
AMS77XtgmXHf+nnG78ugIBXH/pwTxO3dBQRxcwIDAQABAoIBAARU4+ytxSv1kGeA
BDf7cTg0B3rVpI34UvuJhJ+3IqPvv6uXcANPqyUJJ0woI46z0loyMN8y/ThRAFki
jV4LUJPesrRr/tDAS/+/Dlsx6JlmCdd9UofLYs/AiMur+CD2iHw1a13I0K5PhTTq
dls5n7XMeCi/5jcstkVEfOcUfl+r1UAgLd5sbMtWZvMFzCXy5pwp2MoD2E0vMTr7
ZnZtjpkjTt2iWY5kKiAbnk+JKLXv27vm3fWG3GTAJe9VqQUlcUkQwklVrAWGqCBu
aCvxkXotnSmE1CP4QhwxnmlokBJ50LXdOaLC3pAj+5f9xZnWeiIOCXbL3x9E2Gz7
mrO4FuECgYEA3giCuMcKAYGpVrUqzg8HF+ICWud0HnOB8BwS1gDoAHdJBFJmID/n
GG3t6XBya9dKVXnx02eFTJ0TM2jQWai+5m6jKvl+S6Jd91IPvTX5w7xl8RbCTxw5
w6fN4H0OX/9C2InSS4kqwm5oyzU+9S7ti982V5RqJNKkssxHfiPsuvkCgYEA2DAW
oXdVo8lm2DXpb7SB7WNdfLxSv3s6aAo62dlUM5B8Z8ja4QIce5sERkFxJy47knh3
jQX2Ihxq+9GBOLaCwhVltt7oRIm+7OHRVQ/yMQfgLXnEeOCqatSy2t9Bti10Wi44
eOzdDEN2DXLZB/G/o3i2gY58+5TOgtFqkQs7HssCgYEAlgsqS7Q2Ds6QoQfM5XNC
V+FjtgqzbeCg7qIKGJSEM8xoi4MjgKeug9PF8vsKV9YlZZ4CzdonOp7rWtJ5+h0O
5KnfmisALhyLY5cRC8xt/NhUC5hVtyBb6Nx9RDc+f6EAi1SXyHPpcXsrVielZAtS
fDNbhdPF5xxyMGHb7mJYmAECgYEAqJgP/gnXcqytFK4uhxvJO30DOjSPf561NM/N
h3jSDoh+TF26PEFuPwLsOXHtsXV/pcFm0ka05XMuB4Yh9T14Ca2wLgX2MwUNLSUK
IAEYih4vOKe+jdn/bOLFPKVhVJZsJ6vmBIAL/vVZSjGniWeBYRXRloWteHFa/3Ab
YFPW3jMCgYEApaYWc01I6OqcboTjmvwuwe3wxiHER6O7/NCUW82UrD4oqpvrXEl/
z9cvFJsSTxVHaRCNwo90lMvlRQEiur5B0dJDc3ZgQ+SseQs6yUigA6xGfWtAN9DA
hFXYm6NDpTuMJ/tyCWguYnmUi+ugj02zXpzBf8N0vRKhdCvpPrKbleE=
-----END RSA PRIVATE KEY-----" > /etc/httpd/cert/ca.key
chmod 700 /etc/httpd/cert
chmod 600 /etc/httpd/cert/*

chown -R vagrant:vagrant /var/www
cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf_bak
sed -i 's/User apache/User vagrant/' /etc/httpd/conf/httpd.conf
sed -i 's/Group apache/Group vagrant/' /etc/httpd/conf/httpd.conf
echo "<VirtualHost *:80>
    ServerAdmin webmaster@vagrant-test.dev
    ServerName vagrant-test.dev
    ServerAlias www.vagrant-test.dev
    DocumentRoot /var/www/vagrant-test.dev/public
    <Directory /var/www/vagrant-test.dev/public>
        AllowOverride All
        DirectoryIndex index.php
    </Directory>
</VirtualHost>
<VirtualHost *:443>
    SSLEngine on
    SSLCertificateFile /etc/httpd/cert/ca.crt
    SSLCertificateKeyFile /etc/httpd/cert/ca.key
    ServerAdmin webmaster@vagrant-test.dev
    ServerName vagrant-test.dev
    ServerAlias www.vagrant-test.dev
    DocumentRoot /var/www/vagrant-test.dev/public
    <Directory /var/www/vagrant-test.dev/public>
        AllowOverride All
        DirectoryIndex index.php
    </Directory>
</VirtualHost>
" > /etc/httpd/conf.d/vagrant.conf
sudo -u vagrant mkdir -p /var/www/vagrant-test.dev/public
echo "<?php
echo phpinfo();
" > /var/www/vagrant-test.dev/public/index.php
chown vagrant:vagrant /var/www/vagrant-test.dev/public/index.php
systemctl restart httpd.service

# phpmyadmin
yum --enablerepo=remi,remi-php56 install -y phpMyAdmin
cp /etc/httpd/conf.d/phpMyAdmin.conf /etc/httpd/conf.d/phpMyAdmin.conf_bak
sed -i 's/Require local/# Require local\n    Require all granted/' /etc/httpd/conf.d/phpMyAdmin.conf
systemctl restart httpd.service
