#/bin/bash


usage()
{
cat << EOF
--------------------------------------------------------------------------------
usage: $0 version

$0 1.0.1

OPTIONS:
   -h, --help              Show this message
--------------------------------------------------------------------------------
EOF
}

VERSION_FILE=/root/box_version

VERSIONS=(
    '1.0.0'
    '1.0.1'
    '1.0.2'
)

TO_VERSION=$1

if [ -f /vagrant/.version ]; then
    CURRENT_VERSION=`cat $VERSION_FILE`
else
    CURRENT_VERSION=${VERSIONS[0]}
fi

# check if requested version exists
if [ "$TO_VERSION" = "" ] || [ "$TO_VERSION" = "-h" ] || [ "$TO_VERSION" = "--help" ]; then
    usage
    exit
fi
EXISTS=0
for I in ${VERSIONS[@]}; do
    if [ "$TO_VERSION" = "$I" ]; then
        EXISTS=1
        break
    fi
done
if [ $EXISTS = 0 ]; then
    echo "version '$TO_VERSION' do not exists"
    usage
    exit
fi

# check if requested version is same as current
if [ "$TO_VERSION" = "$CURRENT_VERSION" ]; then
    echo "already at $TO_VERSION"
    exit
fi

# check if requested version is older than current
OLDER=0
for I in ${VERSIONS[@]}; do
    if [ "$CURRENT_VERSION" = "$I" ]; then
        OLDER=1
    elif [ "$TO_VERSION" = "$I" ]; then
        if [ $OLDER = 0 ]; then
            OLDER=2
            break
        fi
    fi
done
if [ $OLDER = 2 ]; then
    echo "downgrade not posible. current version is $CURRENT_VERSION"
    exit
fi

# check if user really want upgrade
echo "current version: $CURRENT_VERSION"
echo "upgrade to version: $TO_VERSION"

echo "Continue? [Y/n]"
read a
if [ "$a" = "n" ] || [ "$a" = "N" ]; then
    echo "bye!"
    exit
fi

# run upgrade
RUN=0
for I in ${VERSIONS[@]}; do
    if [ "$CURRENT_VERSION" = "$I" ]; then
        RUN=1
    fi
    if [ $RUN = 2 ]; then
        echo "update to version: $I";
        # bash "/vagrant/setup/update/$I.sh"
    fi
    if [ $RUN = 1 ]; then
        RUN=2
    fi
    if [ "$TO_VERSION" = "$I" ]; then
        break
    fi
done

# remember new version
echo "$TO_VERSION" > $VERSION_FILE

# exit
echo "Done"
