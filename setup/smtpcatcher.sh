#/bin/bash


bash /vagrant/smtpcatcher/perms
sed -i 's/sendmail_path.*/sendmail_path=\/vagrant\/smtpcatcher\/bin\/smtpcatcher/' /etc/php.ini
echo "Alias /smtpcatcher /vagrant/smtpcatcher/www
<Directory /vagrant/smtpcatcher/www>
    Require all granted
</Directory>" > /etc/httpd/conf.d/smtpcatcher.conf
systemctl restart httpd.service
