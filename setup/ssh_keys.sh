#/bin/bash


# https://help.github.com/articles/generating-ssh-keys

echo "processing ssh keys"
if [ ! -f /vagrant/dot/.ssh/config ]; then
    echo "missing ssh keys"
else
    if [ ! -d /home/vagrant/.ssh ]; then
        mkdir /home/vagrant/.ssh
    fi
    if [ -f /home/vagrant/.ssh/config ]; then
        cp /home/vagrant/.ssh/config /home/vagrant/.ssh/config_`date +%Y%m%d_%H%M%S`_bak
    fi
    cp -r /vagrant/dot/.ssh/* /home/vagrant/.ssh
    chown -R vagrant:vagrant /home/vagrant/.ssh
    find /home/vagrant/.ssh -type d -exec chmod 0700 {} \;
    find /home/vagrant/.ssh -type f -exec chmod 0600 {} \;
fi
