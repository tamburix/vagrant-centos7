#/bin/bash


sudo -u vagrant mkdir /var/www/logs

yum install -y nodejs npm

if [ ! -f /etc/php.d/15-xdebug.ini ]; then
    echo "missing xdebug init /etc/php.d/15-xdebug.ini"
else
    echo "xdebug.overload_var_dump = 0
xdebug.remote_enable=on
xdebug.remote_log=\"/var/log/xdebug.log\"
xdebug.remote_host=192.168.0.100
xdebug.remote_handler=dbgp
xdebug.remote_port=9000
" >> /etc/php.d/15-xdebug.ini
    systemctl restart httpd.service
fi
