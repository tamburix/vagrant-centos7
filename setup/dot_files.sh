#/bin/bash

sudo -u vagrant cp -r /vagrant/dot/.vim /home/vagrant
cp -r /vagrant/dot/.vim /root

sudo -u vagrant cp -r /vagrant/dot/.vimrc /home/vagrant
cp -r /vagrant/dot/.vimrc /root

sudo -u vagrant cp -rf /vagrant/dot/.gitconfig /home/vagrant
cp -rf /vagrant/dot/.gitconfig /root

sudo -u vagrant cp -rf /vagrant/dot/.bashrc_vagrant /home/vagrant/.bashrc
cp -rf /vagrant/dot/.bashrc_root /root/.bashrc
