#/bin/bash


systemctl stop firewalld.service
systemctl disable firewalld.service

if [ ! -d /var/www ]; then
    mkdir /var/www
fi
chown -R vagrant:vagrant /var/www
yum install -y nfs*
echo "/var/www 192.168.33.0/24(rw,sync,all_squash,anonuid=1000,anongid=1000)" > /etc/exports
systemctl enable rpcbind.service
systemctl enable nfs-server.service
systemctl start rpcbind.service
systemctl start nfs-server.service
