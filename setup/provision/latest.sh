#/bin/bash


echo "provision..."

bash /vagrant/setup/upgrade/1.0.1.sh
bash /vagrant/setup/upgrade/1.0.2.sh

bash /vagrant/setup/ssh_keys.sh
bash /vagrant/setup/set_random_git_author.sh

echo "1.0.2" > /root/box_version


echo "-------------------------------------------------------------------------"
echo "VERSION 1.0.2"
echo " "
echo "happy hacking ;)"
echo "-------------------------------------------------------------------------"
