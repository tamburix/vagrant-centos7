#/bin/bash


rm -f /etc/localtime && cp -f /usr/share/zoneinfo/Europe/Belgrade /etc/localtime

yum install -y git unzip man vim wget nodejs npm ruby ruby-devel rubygems bash-completion
gem install compass

bash /vagrant/setup/dot_files.sh
bash /vagrant/setup/nfs.sh
bash /vagrant/setup/lamp.sh


echo
echo "happy hacking ;)"
